import requests # call api
import urllib.request # download videos
import os, time, csv, sys
import cv2

from PIL import Image
import numpy as np

from imageUtil import ImageUtil
from dbscanner import DBScanner

NUMBER_SECOND_TO_ADD = 1

hosts_url = "https://localhost:9011"
suffix_url_videos = "/api/speach/videos"
url_videos = hosts_url + suffix_url_videos
suffix_url_photo = "/api/user/allPhotoProfil"
url_photo = hosts_url + suffix_url_photo
suffix_url_video_webcam = "/api/speach/videosWebcam"
url_videos_webcam = hosts_url + suffix_url_video_webcam


# todo jouer avec conf
videos_folder = "videos"
images_folder = "tmp"
images_croped_folder="croped"
files_folder = "files"
csv_fileName = "vectorization_images_cropped.csv"
clusters_folder = "clusters"

ImageUtil.clearAndCreatFolder(videos_folder)
ImageUtil.clearAndCreatFolder(images_folder)

# Recuperation des images de profils
# recupere les liens des images
#ImageUtil.clearAndCreatFolder(image_profil_folder)
photos_urls_response = requests.get(url_photo, verify=False)
photos_urls_dictionnary = photos_urls_response.json()

count = 0
for personne in photos_urls_dictionnary:
    imagePath = photos_urls_dictionnary[personne]

    imageName = personne + "_" + str(count) + ".png"
    image = os.path.join(images_folder, imageName)

    urllib.request.urlretrieve(imagePath, image)

    count += 1

def extract_image_externe_video(videos_urls):
    def videoTime():
        currentVideoTime = video_capture.get(cv2.CAP_PROP_POS_MSEC)
        nextVideoTime = currentVideoTime + (NUMBER_SECOND_TO_ADD * 1000)

        video_capture.set(cv2.CAP_PROP_POS_MSEC, nextVideoTime )
        return nextVideoTime

    # recuepre les videos
    count = 0
    for video_url in videos_urls:
        if video_url.endswith(".mp4"):
            video_name = video_url[video_url.rfind("/")+1:]
        else :
            name = video_url
            video_url = videos_urls[video_url]
            video_name = name + video_url[video_url.index("."):]

        video = os.path.join(videos_folder, video_name)
        urllib.request.urlretrieve(video_url, video)

        count += 1

        # RECUPERATION DES IMAGES DANS LES VIDEOS + Extraction de visage
        currentVideoTime = 0
        frameRate = 0
        nbTotalFrame = 0
        videoLenght = 0
        numImage = 0

        cascPath = os.path.join("requirement", "haarcascade_frontalface_default.xml")
        faceCascade = cv2.CascadeClassifier(cascPath)

        # Doc :  https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html?highlight=videocapture#cv2.VideoCapture.get
        video_capture = cv2.VideoCapture(video)

        if video_capture.isOpened():
            nbTotalFrame = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
            frameRate = video_capture.get(cv2.CAP_PROP_FPS)
            #print("frameRate : ", frameRate)

            #print("nombre total de frame dans la video : ", nbTotalFrame)
            videoLenght = (nbTotalFrame/frameRate)

            #print("video length : ", videoLenght, "s")

        while (videoLenght * 1000) > currentVideoTime: # *1000 --> conversion en miliseconde
            if not video_capture.isOpened():
                print('Unable to load camera.')
                time.sleep(5)
                pass

            # Capture frame-by-frame
            ret, frame = video_capture.read()

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            # si une erreur survient sur une image, on passe a la suivante
            if not ret:
                print("Une erreur est surevenu sur l'image")
                print("temps : ", video_capture.get(cv2.CAP_PROP_POS_MSEC))
                print("passage a l'image suivante")
                currentVideoTime = videoTime()
                continue

            # convert to gray pour que l'image soit reconnaisable par FaceCascade --> cf cascPAth
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            faces = faceCascade.detectMultiScale(
                gray,
                scaleFactor=1.1,
                minNeighbors=5,
                minSize=(30, 30)
            )

            if len(faces) > 0:

                # Draw a rectangle around the faces
                '''
                for (x, y, w, h) in faces:
                    cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

                if anterior != len(faces):
                    anterior = len(faces)
                    #log.info("faces: "+str(len(faces))+" at "+str(dt.datetime.now()))

                # Display the resulting frame
                cv2.imshow('Video', frame)
                '''
                # save image
                fileName = video_name[:video_name.rfind(".")] + "_img" + str(numImage) + ".jpg" # extension obligatoire sinon openCV ne sait pas enregister l'image
                output = os.path.join(images_folder, fileName)
                cv2.imwrite(output, frame)
                numImage = numImage + 1

            # Passage a l'image suivante
            currentVideoTime = videoTime()

#print("url : ", url_videos)
# recupere les liens des videos
videos_urls_response = requests.get(url_videos, verify=False)
videos_urls_array = videos_urls_response.json()


videos_urls_response = requests.get(url_videos_webcam, verify=False)
videos_urls_webcam_dictionary = videos_urls_response.json()
extract_image_externe_video(videos_urls_webcam_dictionary)
extract_image_externe_video(videos_urls_array)

# traitement des images
# PreProcessing --> garde que les images avec des tetes détecté
ImageUtil.clearAndCreatFolder(images_croped_folder)
ImageUtil.formatImages(images_folder, images_croped_folder)

# DBSCAN --> clusterisation
# src : https://github.com/SushantKafle/DBSCAN.git
allImages = ImageUtil.getAllImages(images_croped_folder)

# Gestion du CSV
ImageUtil.createFolder(files_folder)

csv_file = os.path.join(files_folder,csv_fileName)
o_csv_file = open(csv_file,"w")
# todo mettre dans les confs le delimiter
writer = csv.writer(o_csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONE, escapechar='\\')

imageArray = []
for imagePath in allImages:
    Im = Image.open(imagePath)
    pixel = Im.load()
    width, height = Im.size

    imageArray = []
    for x in range(0, width):
        for y in range(0, height):
            imageArray.append(pixel[y, x])

    npImageArray = np.array(imageArray)

    writer.writerow( npImageArray )

o_csv_file.close()

# Read CSV
def get_data(config, csv_file):
    data = []
    with open(csv_file, 'r') as file_obj:
        csv_reader = csv.reader(file_obj)
        for id_, row in enumerate(csv_reader):
            if len(row) < config['dim']:
                print ("ERROR: The data you have provided has fewer \
                    dimensions than expected (dim = %d < %d)"
                    % (config['dim'], len(row)))
                sys.exit()
            else:
                point = {'id':id_}
                for dim in range(0, config['dim']):
                    point[dim] = float(row[dim])
                data.append(point)
    return data

# todo mettre dans config
config_dbscan = {
    "eps": 1200,
    "dim": 784,
    "min_pts": 2
}

dbc = DBScanner(config_dbscan)
data = get_data(config_dbscan, csv_file)

dbc.dbscan(data)

ImageUtil.clearAndCreatFolder(clusters_folder)
separator = "\\" if sys.platform == "win32" else "/"
# affiche les ID pour pouvoir retrouver les images d'origine
for cluster in dbc.clusters:
    destinationFolder = os.path.join(clusters_folder, cluster.name)

    #if cluster.name != "Noise":
    ImageUtil.createFolder(destinationFolder)
    for point in cluster.points:
        id = point["id"]

        imagePath = allImages[id]
        imageName = imagePath[imagePath.index(separator)+1:]
        destinationFile = os.path.join(destinationFolder, imageName)

        os.rename(imagePath, destinationFile)


# delete folder
#ImageUtil.clearFolder(images_folder)
#ImageUtil.clearFolder(images_croped_folder)
ImageUtil.clearFolder(videos_folder)

# renomer le cluster
print("todo : renomer les clusters")
