import os
import sys

import cv2
import random

from config import Config

class ImageUtil:

    # clear
    @staticmethod
    def clearFolder(directory):
        if os.path.exists(directory):
            for dirPath, dirNames, files in os.walk(directory):
                for file in files:
                    filePath = os.path.join(dirPath, file)
                    #fd = os.open( file, os.O_RDWR|os.O_CREAT )
                    #os.close( fd )

                    os.remove(filePath)

                for dirName in dirNames:
                    ImageUtil().clearFolder( os.path.join(dirPath, dirName) )

            os.rmdir(directory)

    # createFolder
    # verifie l'existance du dossier test si non le crée
    @staticmethod
    def createFolder(folder):
        if not os.path.exists(folder):
            os.makedirs(folder)

    # todo make comment
    @staticmethod
    def clearAndCreatFolder(folder):
        ImageUtil.clearFolder(folder)
        ImageUtil.createFolder(folder)

    # Met les images dans le format attendu
    # GrayScale, Crop, Resize, Convertion en JPG
    @staticmethod
    def formatImages(folderInput, folderOutput):
        folderTmp = Config.getImagePathConfig(["tmp"])[0]

        ImageUtil.clearAndCreatFolder(folderTmp)
        ImageUtil.clearAndCreatFolder(folderOutput)

        ImageUtil.detectFacesFolder(folderInput, folderTmp)
        ImageUtil.detectFacesFolder(folderTmp, folderOutput)

        ImageUtil.clearFolder(folderTmp)

    # todo make comment
    @staticmethod
    def detectFacesFolder(input, output):
        for dirPath, dirNames, files in os.walk(input):
            for dirName in dirNames:
                ImageUtil.detectFacesFolder(dirName, output)

            ImageUtil.detectFacesFiles(dirPath, output)

    # todo make comment
    @staticmethod
    def detectFacesFiles(currentDirectory, output):
        CASC_PATH = os.path.join( Config().getDetectFacesConfig(["folder"])[0], Config().getDetectFacesConfig(["file"])[0] )
        faceCascade = cv2.CascadeClassifier(CASC_PATH)

        separator = "\\" if sys.platform == "win32" else "/"

        extension = Config().getImageConfig(["extension"])[0]

        for dirPath, dirNames, files in os.walk(currentDirectory):
            for file in files:
                if file.startswith("."):
                    continue
                filePath = os.path.join(dirPath,file)
                imgRead = cv2.imread(filePath)

                # GreyScale
                imgRead = cv2.cvtColor(imgRead, cv2.COLOR_BGR2GRAY)

                # recherche de visage
                try :
                    faces = faceCascade.detectMultiScale(
                        imgRead,
                        scaleFactor=1.1,
                        minNeighbors=5,
                        minSize=(30, 30)
                    )
                except:
                    faces = []

                if len(faces) > 0:
                    ImageUtil.createFolder(output)

                    img = imgRead
                    count = 0

                    # Draw a rectangle around the faces
                    for (x, y, w, h) in faces:
                        fileName = filePath[filePath.rfind(separator)+1:filePath.index(".")] + "_" + str(count) + extension
                        destinationFile = os.path.join(output, fileName)

                        imgRead = imgRead[y:y+h, x:x+w]
                        if output != Config().getImagePathConfig(["tmp"])[0]:
                            imgRead = cv2.resize(imgRead, (Config().getImageConfig(["width"])[0], Config().getImageConfig(["height"])[0]), interpolation = cv2.INTER_NEAREST)

                        cv2.imwrite(destinationFile, imgRead)
                        imgRead = img
                        count += 1
    # todo make comment
    @staticmethod
    def splitTrainEval(folderInput, folderOutput):
        print("folderOutput : ", folderOutput)
        ImageUtil.clearAndCreatFolder(folderOutput)

        allImages = ImageUtil.getAllImages(folderInput)
        random.shuffle(allImages)
        print("allImages len : ", len(allImages))

        separator = "\\" if sys.platform == "win32" else "/"

        percentage_train_test = Config.getDeepLearningConfig()[0]
        numberTrain = int( len(allImages) * percentage_train_test / 100 )

        for i in range(len(allImages)):
            imagePath = allImages[i]

            if i < numberTrain:
                folder = os.path.join(folderOutput, "train")
            else:
                folder = os.path.join(folderOutput, "test")
            ImageUtil.createFolder(folder)

            filePath = imagePath.replace(folderInput, folder)

            subFolderName = filePath.split(separator)[-2]
            if subFolderName != "train" and subFolderName != "test":
                subFolder = os.path.join( folder, subFolderName )
                ImageUtil.createFolder( subFolder )
            print("imagePath : ", imagePath)
            print("filePath : ", filePath)
            os.rename(imagePath, filePath)

    # todo make comment
    @staticmethod
    def getAllImages(folder):
        images = []
        for dirPath, dirNames, files in os.walk(folder):
            for dirName in dirNames:
                files = ImageUtil.getAllImages(dirName)
                images.extend(files)

            files = ImageUtil.getAllFiles(dirPath)
            images.extend(files)

        """
        for directoryName in os.listdir(folder):
            if directoryName.startswith("."):
                continue
            currentDirectory = os.path.join(folder, directoryName)

            for subDirPath, subDirNames, subFiles in os.walk(currentDirectory):
                for file in subFiles:
                    if file.startswith("."):
                        continue
                    filePath = os.path.join(subDirPath,file)

                    images.append(filePath)
        """
        return images

    # todo make comment
    def getAllFiles(folder):
        images = []
        for dirPath, dirNames, files in os.walk(folder):
            for file in files:
                if file.startswith("."):
                    continue
                filePath = os.path.join(dirPath,file)

                images.append(filePath)
        return images
