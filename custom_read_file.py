# reference :  https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/learn/python/learn/datasets/mnist.py
import os
import gzip
import numpy

from tensorflow.python.framework import dtypes
from tensorflow.python.platform import gfile
from tensorflow.contrib.learn.python.learn.datasets.mnist import DataSet
from tensorflow.contrib.learn.python.learn.datasets import base

from config import Config

class CustomReadFile:

    def __init__(self):
        self.num_classes = self.getNumberClasses()

    def getNumberClasses(self):
        folder_images_path = "output"
        return len(os.listdir(folder_images_path))

    def read_data_sets(self, one_hot=False, dtype=dtypes.float32, reshape=True, seed=None):
        folder = Config.getFilesConfig(["folder"])[0]
        # todo train et test doivent venir de config
        fileName = Config.getFilesConfig(["images"])[0].format("train") + ".gz"
        TRAIN_IMAGES = os.path.join(folder, fileName)

        fileName = Config.getFilesConfig(["labels"])[0].format("train") + ".gz"
        TRAIN_LABELS = os.path.join(folder, fileName)

        fileName = Config.getFilesConfig(["images"])[0].format("test") + ".gz"
        TEST_IMAGES = os.path.join(folder, fileName)

        fileName = Config.getFilesConfig(["labels"])[0].format("test") + ".gz"
        TEST_LABELS = os.path.join(folder, fileName)

        local_file = TRAIN_IMAGES
        with gfile.Open(local_file, 'rb') as f:
            train_images = self.extract_images(f)

        local_file = TRAIN_LABELS
        with gfile.Open(local_file, 'rb') as f:
            train_labels = self.extract_labels(f, one_hot=one_hot)

        local_file = TEST_IMAGES
        with gfile.Open(local_file, 'rb') as f:
            test_images = self.extract_images(f)

        local_file = TEST_LABELS
        with gfile.Open(local_file, 'rb') as f:
            test_labels = self.extract_labels(f, one_hot=one_hot)

        '''
        train_images = train_images.astype(numpy.float16)
        train_labels = train_labels.astype(numpy.int32)
        test_images = test_images.astype(numpy.float16)
        test_labels = test_labels.astype(numpy.int32)
        '''
        options = dict(dtype=dtype, reshape=reshape, seed=seed)

        train = DataSet(train_images, train_labels, **options)
        validation = None
        test = DataSet(test_images, test_labels, **options)

        return base.Datasets(train=train, validation=validation, test=test)

    # todo make comment
    def read_predict(self, dtype=dtypes.float32, reshape=True, seed=None):

        folder = Config.getFilesConfig(["folder"])[0]
        # predict ne doit pas etre ecrit en dure --> todo config
        fileName = Config.getFilesConfig(["images"])[0].format("predict")
        PREDICT_IMAGES = os.path.join(folder, fileName)
        print("predict_images : ", PREDICT_IMAGES)
        exit()

        local_file = PREDICT_IMAGES
        with gfile.Open(local_file, 'rb') as f:
            predict_images = self.extract_images(f)

        predict_labels = numpy.arange(len(predict_images)) # On s'en fou des labels ici

        options = dict(dtype=dtype, reshape=reshape, seed=seed)

        predict = DataSet(predict_images, predict_labels, **options)

        return base.Datasets(train=predict,validation=None, test=None)

    # todo make comment
    def extract_images(self, f):
        """Extract the images into a 4D uint8 numpy array [index, y, x, depth].
        Args:
        f: A file object that can be passed into a gzip reader.
        Returns:
        data: A 4D uint8 numpy array [index, y, x, depth].
        Raises:
        ValueError: If the bytestream does not start with 2051.
        """
        print('Extracting', f.name)
        with gzip.GzipFile(fileobj=f) as bytestream:
            magic = self._read32(bytestream)
            if magic != 2051:
              raise ValueError('Invalid magic number %d in MNIST image file: %s' %
                               (magic, f.name))
            num_images = self._read32(bytestream)
            rows = self._read32(bytestream)
            cols = self._read32(bytestream)
            buf = bytestream.read(rows * cols * num_images)
            data = numpy.frombuffer(buf, dtype=numpy.uint8)
            data = data.reshape(num_images, rows, cols, 1)
            return data

    # todo make comment
    def extract_labels(self, f, one_hot=False, num_classes=None):
        """Extract the labels into a 1D uint8 numpy array [index].
        Args:
        f: A file object that can be passed into a gzip reader.
        one_hot: Does one hot encoding for the result.
        num_classes: Number of classes for the one hot encoding.
        Returns:
        labels: a 1D uint8 numpy array.
        Raises:
        ValueError: If the bystream doesn't start with 2049.
        """
        if num_classes is None:
            num_classes = self.num_classes

        print('Extracting', f.name)
        with gzip.GzipFile(fileobj=f) as bytestream:
            magic = self._read32(bytestream)
            if magic != 2049:
              raise ValueError('Invalid magic number %d in MNIST label file: %s' %
                               (magic, f.name))
            num_items = self._read32(bytestream)
            buf = bytestream.read(num_items)
            labels = numpy.frombuffer(buf, dtype=numpy.uint8)

            if one_hot:
              return dense_to_one_hot(labels, num_classes)
            return labels

    # tod make comment
    def _read32(self,bytestream):
        dt = numpy.dtype(numpy.uint32).newbyteorder('>')
        return numpy.frombuffer(bytestream.read(4), dtype=dt)[0]
