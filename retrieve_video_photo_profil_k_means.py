import requests # call api
import urllib.request # download videos
import os, time, csv, sys
import cv2

from PIL import Image
import numpy as np

from imageUtil import ImageUtil
from dbscanner import DBScanner

NUMBER_SECOND_TO_ADD = 1

hosts_url = "https://localhost:9011"
suffix_url_videos = "/api/speach/videos"
url_videos = hosts_url + suffix_url_videos
suffix_url_photo = "/api/user/allPhotoProfil"
url_photo = hosts_url + suffix_url_photo


# todo jouer avec conf
videos_folder = "videos"
images_folder = "tmp"
images_croped_folder="croped"
files_folder = "files"
csv_fileName = "vectorization_images_cropped.csv"
clusters_folder = "clusters"

"""
def videoTime():
    currentVideoTime = video_capture.get(cv2.CAP_PROP_POS_MSEC)
    nextVideoTime = currentVideoTime + (NUMBER_SECOND_TO_ADD * 1000)

    video_capture.set(cv2.CAP_PROP_POS_MSEC, nextVideoTime )
    return nextVideoTime

ImageUtil.clearAndCreatFolder(videos_folder)
ImageUtil.clearAndCreatFolder(images_folder)

# Recuperation des images de profils
# recupere les liens des images
#ImageUtil.clearAndCreatFolder(image_profil_folder)
photos_urls_response = requests.get(url_photo, verify=False)
photos_urls_dictionnary = photos_urls_response.json()

count = 0
for personne in photos_urls_dictionnary:
    imagePath = photos_urls_dictionnary[personne]

    imageName = personne + "_" + str(count) + ".png"
    image = os.path.join(images_folder, imageName)

    urllib.request.urlretrieve(imagePath, image)

    count += 1

#print("url : ", url_videos)
# recupere les liens des videos
videos_urls_response = requests.get(url_videos, verify=False)
videos_urls_array = videos_urls_response.json()

# recuepre les videos
count = 0
for video_url in videos_urls_array:
    video_name = "videos" + str(count) + ".mp4"

    video = os.path.join(videos_folder, video_name)
    urllib.request.urlretrieve(video_url, video)

    count += 1

    # RECUPERATION DES IMAGES DANS LES VIDEOS + Extraction de visage
    currentVideoTime = 0
    frameRate = 0
    nbTotalFrame = 0
    videoLenght = 0
    numImage = 0

    cascPath = os.path.join("requirement", "haarcascade_frontalface_default.xml")
    faceCascade = cv2.CascadeClassifier(cascPath)

    # Doc :  https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html?highlight=videocapture#cv2.VideoCapture.get
    video_capture = cv2.VideoCapture(video)

    if video_capture.isOpened():
        nbTotalFrame = video_capture.get(cv2.CAP_PROP_FRAME_COUNT)
        frameRate = video_capture.get(cv2.CAP_PROP_FPS)
        #print("frameRate : ", frameRate)

        #print("nombre total de frame dans la video : ", nbTotalFrame)
        videoLenght = (nbTotalFrame/frameRate)

        #print("video length : ", videoLenght, "s")

    while (videoLenght * 1000) > currentVideoTime: # *1000 --> conversion en miliseconde
        if not video_capture.isOpened():
            print('Unable to load camera.')
            time.sleep(5)
            pass

        # Capture frame-by-frame
        ret, frame = video_capture.read()

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        # si une erreur survient sur une image, on passe a la suivante
        if not ret:
            print("Une erreur est surevenu sur l'image")
            print("temps : ", video_capture.get(cv2.CAP_PROP_POS_MSEC))
            print("passage a l'image suivante")
            currentVideoTime = videoTime()
            continue

        # convert to gray pour que l'image soit reconnaisable par FaceCascade --> cf cascPAth
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30)
        )

        if len(faces) > 0:

            # Draw a rectangle around the faces
            '''
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

            if anterior != len(faces):
                anterior = len(faces)
                #log.info("faces: "+str(len(faces))+" at "+str(dt.datetime.now()))

            # Display the resulting frame
            cv2.imshow('Video', frame)
            '''
            # save image
            fileName = video_name[:video_name.rfind(".")] + "_img" + str(numImage) + ".jpg" # extension obligatoire sinon openCV ne sait pas enregister l'image
            output = os.path.join(images_folder, fileName)
            cv2.imwrite(output, frame)
            numImage = numImage + 1

        # Passage a l'image suivante
        currentVideoTime = videoTime()

# traitement des images
# PreProcessing --> garde que les images avec des tetes detecte
ImageUtil.clearAndCreatFolder(images_croped_folder)
ImageUtil.formatImages(images_folder, images_croped_folder)
"""
# k_means --> clusterisation
# src : https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/2_BasicModels/kmeans.py

nb_clusters_expexted = len(photos_urls_dictionnary) + 1
print("nb_clusters : ", nb_clusters_expexted)
