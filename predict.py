import tensorflow as tf
from datetime import datetime
import json

from config import Config
from imageUtil import ImageUtil
from convert_images_to_mnist_format import ConvertImageToMnistFormat
from custom_read_file import CustomReadFile
from model import Model

begin = datetime.now()
tf.logging.set_verbosity(tf.logging.INFO)

def main(unused_argv):
    # PreProcessing --> garde que les images avec des tetes détecté
    input = Config.getImagePathConfig(["predict"])[0]
    output = Config.getImagePathConfig(["output"])[0]
    ImageUtil().formatImages(input, output)

    # Convertie les images dans un fichier .gz
    ConvertImageToMnistFormat(True)

    # Load predict
    dataset_predict = CustomReadFile().read_predict()
    predict_images = dataset_predict.train.images


    # Create the Estimator
    model_dir = Config.getDeepLearningConfig(["model_dir"])[0]
    mnist_classifier = tf.estimator.Estimator(model_fn=Model.cnn_model_fn, model_dir=model_dir)

    # Set up logging for predictions
    # Log the values in the "Softmax" tensor with label "probabilities"
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=50)

    # Train the model
    predict_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": predict_images},
        num_epochs=1,
        shuffle=False)

    predictions = list( mnist_classifier.predict( input_fn=predict_input_fn ) )

    predicted_classes = [p["classes"] for p in predictions]

    print( "New Samples, Class Predictions:  {}\n" .format(predicted_classes) )

    # todo voir output expected
    """
    json_data = open("files\labels.txt").read()
    labels = json.loads(json_data)
    for index in predicted_classes:
        print("name : ", labels[index])
    """

    end = datetime.now()
    print("execution time : ", (end - begin))


if __name__ == "__main__":
  tf.app.run()
