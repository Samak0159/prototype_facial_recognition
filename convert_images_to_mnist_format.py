import os
import sys

from PIL import Image
from array import *
from random import shuffle
import json

from config import Config
from imageUtil import ImageUtil


class ConvertImageToMnistFormat:

    # clear
    def clearFolder(self,directory):
        if os.path.exists(directory):
            for dirPath, dirNames, files in os.walk(directory):
                for file in files:
                    filePath = os.path.join(dirPath,file)
                    fd = os.open( "foo.txt", os.O_RDWR|os.O_CREAT )
                    os.close( fd )

                    os.remove(filePath)

                for dirName in dirNames:
                    self.clearFolder( os.path.join(dirPath,dirName) )

            os.rmdir(directory)

    # Constructeur de la class, permet de lancer soit le TrainEval soit le predict
    def __init__(self, isPredict = False):
        # Load from
        if isPredict:
            self.convertPredict()
        else:
            self.convertTrainEval()

    # Recupere les images et les labels pour les enregistrer dans un GZ
    def convertTrainEval(self):
        # train
        imagePathConfig = Config.getImagePathConfig(["dataset", "datasetTrain", "datasetTest"])

        trainFolder = os.path.join(imagePathConfig[0], imagePathConfig[1])
        testFolder = os.path.join(imagePathConfig[0], imagePathConfig[2])

        Names = [[ trainFolder, 'train'], [ testFolder, 'test']]

        # generate names
        self.getNames(ImageUtil.getAllFiles(imagePathConfig[0]))

        for name in Names:

            data_image = array('B')
            data_label = array('B')

            FileList = ImageUtil.getAllFiles(name[0])

            """
            for dirname in os.listdir(name[0]): # [1:] Excludes .DS_Store from Mac OS
                if dirname.startswith(".D"):
                    continue

                path = os.path.join(name[0], dirname)
                #for filename in os.listdir(path):
                for dirPath, dirNames, files in os.walk(path):
                    for filename in files:
                        if filename.endswith(".png"):
                            FileList.append(os.path.join(name[0], dirname, filename))
            """
            shuffle(FileList) # Usefull for further segmenting the validation set

            # recuperation des names
            names = self.getNames(FileList)

            for filename in FileList:
                personne_name = self.getPersonneName(filename)
                label = names.index(personne_name)

                Im = Image.open(filename)

                pixel = Im.load()

                width, height = Im.size

                for x in range(0, width):
                    for y in range(0, height):
                        data_image.append(pixel[y, x])

                data_label.append(label) # labels start (one unsigned byte each)

            hexval = "{0:#0{1}x}".format(len(FileList), 6) # number of files in HEX

            # header for label array

            header = array('B')
            header.extend([0, 0, 8, 1, 0, 0])
            header.append(int('0x' + hexval[2:][:2], 16))
            header.append(int('0x' + hexval[2:][2:], 16))

            data_label = header + data_label

            # additional header for images array

            if max([width, height]) <= 256:
                header.extend([0, 0, 0, width, 0, 0, 0, height])
            else:
                raise ValueError('Image exceeds maximum size: 256x256 pixels')

            header[3] = 3  # Changing MSB for image data (0x00000803)

            data_image = header + data_image

            fileConfig = Config.getFilesConfig()
            folder = fileConfig[0]

            # images
            outputFileName = os.path.join(folder, fileConfig[1].format(name[1]))
            output_file = open(outputFileName, 'wb')
            data_image.tofile(output_file)
            output_file.close()

            # labels
            outputFileName = os.path.join(folder, fileConfig[2].format(name[1]))
            output_file = open(outputFileName, 'wb')
            data_label.tofile(output_file)
            output_file.close()

        # gzip resulting files
        # Enregistre les images et les labels dans des fichier gzip
        for name in Names:
            os.system('gzip ' + os.path.join(folder, fileConfig[1].format(name[1])))
            os.system('gzip ' + os.path.join(folder, fileConfig[2].format(name[1])))

    # Prmet de lire les images pour la prediction. Les enregistres dans un fichier gzip
    def convertPredict(self):
        # train
        predictFolder = Config.getImagePathConfig(["output"])[0]
        print("predictFolder : ", predictFolder)

        data_image = array('B')

        FileList = []
        for dirname in os.listdir( predictFolder ):  # [1:] Excludes .DS_Store from Mac OS
            if dirname.startswith("."):
                continue

            path = os.path.join( predictFolder, dirname)
            for filename in os.listdir(path):
                if filename.endswith(".png"):
                    FileList.append(os.path.join( predictFolder, dirname, filename))

        for filename in FileList:
            Im = Image.open(filename)

            pixel = Im.load()

            width, height = Im.size

            for x in range(0, width):
                for y in range(0, height):
                    data_image.append(pixel[y, x])

            hexval = "{0:#0{1}x}".format(len(FileList), 6)  # number of files in HEX

            # header for label array

            header = array('B')
            header.extend([0, 0, 8, 1, 0, 0])
            header.append(int('0x' + hexval[2:][:2], 16))
            header.append(int('0x' + hexval[2:][2:], 16))

            # additional header for images array

            if max([width, height]) <= 256:
                header.extend([0, 0, 0, width, 0, 0, 0, height])
            else:
                raise ValueError('Image exceeds maximum size: 256x256 pixels')

            header[3] = 3  # Changing MSB for image data (0x00000803)

            data_image = header + data_image

            # images
            folder = Config.getFilesConfig(["folder"])[0]
            # todo predict ne doit pas etre en dure passer par une conf
            fileName = Config.getFilesConfig(["images"])[0].format("predict")
            outputFileName = os.path.join(folder, fileName)
            output_file = open(outputFileName, 'wb')
            data_image.tofile(output_file)
            output_file.close()

        # gzip resulting files
        folder = Config.getFilesConfig(["folder"])[0]
        # todo predict ne doit pas etre en dure passer par une conf
        fileName = Config.getFilesConfig(["images"])[0].format("predict")

        os.system('gzip ' + os.path.join(folder, fileName) )

    # Recupere tous les noms des personnes
    def getNames(self, files):
        names = self.loadNames()

        if names is not None:
            return names
        else:
            names = []

            for filePath in files:
                name = self.getPersonneName(filePath)

                if name not in names:
                    names.append(name)

            self.saveNames(names)

            return names

    # Charge les noms issus du fichier
    def loadNames(self):
        # todo names utiliser config
        filename = os.path.join("files", "labels.txt")

        if not os.path.isfile(filename):
            return None
        else:
            json_data = open(filename).read()
            names = json.loads(json_data)

            return names

    # Enregistrer les noms dans un fichier
    def saveNames(self, names):
        # todo utiliser config
        ImageUtil.createFolder("files")
        filename = os.path.join("files", "labels.txt")
        with open(filename, 'w') as file:
            file.write(json.dumps(names))  # use `json.loads` to do the reverse

        return names

    # Extrait le nom des personnes depuis le chemin relatif ou absolue de l'image.
    # Le nom doit etre le dernier dossier contenant l'image
    def getPersonneName(self, filename):
        separator = "\\" if sys.platform == "win32" else "/"
        return filename.replace("_", "").split(separator)[-2]
