# To run
1. Convertir les images en png : `python transform_image_to_png.py`
2. Ecrit les images dans un ficher gz : `python convert_image_to_mnist_format`
3. Entraîner l'ia : `python main.py`

# Requirement
* tester avec python 3.6
* tensorflow 1.7 : pip install tensorflow-gpu
* numpy
* cv2
* matplotlib : pip install --upgrade matplotlib
* scipy : pip install scipy


# Sources
* https://github.com/gskielian/JPG-PNG-to-MNIST-NN-Format/blob/master/convert-images-to-mnist-format.py
* https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/learn/python/learn/datasets/mnist.py
* http://yann.lecun.com/exdb/mnist/
* https://hackernoon.com/4-ways-to-manage-the-configuration-in-python-4623049e841b

# Bug
* Que se passe-t-il si il y a deux personnes avec le meme nom

# check video information
* ouvrir la video avec VLC (verison 2.2.8)
* window > Media Inforation (command + I)
* Codec Detail
* lire...
